---
layout: handbook-page-toc
title: "Yearlies"
description: ""
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Yearlies

Yearlies are the annual goals for the company. 

## Alignment to Three Year Strategy 

- Yearlies are informed by the [three-year strategy](https://about.gitlab.com/company/strategy/). 
- Like [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/), each yearly is aligned to one of the three pillars of the [three year strategy](/company/strategy/#three-year-strategy). 
- Each strategic pillar from [three year strategy](/company/strategy/#three-year-strategy) is aligned to at least one yearly.

## Difference between OKRs and Yearlies

- [OKRs](https://about.gitlab.com/company/okrs/) are composed of Objectives and Key Results while Yearlies have only one component, the annual goal.
- [OKRs](https://about.gitlab.com/company/okrs/) have a duration of one quarter while Yearlies are annual goals with a duration of a year.

## Cadence

- The [three year strategy](/company/strategy/#three-year-strategy) is on a 3 year cadence and is inspiration for the Yearlies, which are on a 1 year [cadence](/company/cadence/#year). 

- Yearlies are updated during E-Group offsite as  once-a-year topic in the [offsite topic calendar](https://about.gitlab.com/company/offsite/#offsite-topic-calendar). 

- From the time they are established by E-Group, Yearlies are the company's goals for the next 12 months or until the Yearly is achieved, whichever comes first.

## FY24 Yearlies


### 1. Customer Results 
 
    1. Establish GitLab as default DevSecOps offering for enterprise customers by onboarding xx customers to Dedicated 
    1. Drive DevSecOps Adoption for our customers in order to increase Ultimate NetARR by $X through uptiers   
    1. FedRAMP

### 2. Mature the Platform 
    1. 5 categories to be mature
    1. 
    1.

### 3. Grow Careers 
    1. 
    1. 
    1.     